from flask import Flask
from config import Config
from datetime import timedelta
from flask_pymongo import PyMongo
import os
from werkzeug.utils import secure_filename
from flask_cors import CORS
from flask import render_template

latteh_app = Flask(__name__,
                static_url_path='',
                static_folder='static',
                template_folder='template')
CORS(latteh_app)
latteh_app.config.from_object(Config)
latteh_app.config['PERMANENT_SESSION_LIFETIME']=timedelta(minutes=60)
mongodb_client = PyMongo(latteh_app, uri=latteh_app.config['DB_DSN'])
os.makedirs(os.path.join(latteh_app.config['USER_PROFILE_PHOTO']), exist_ok=True)
DB=mongodb_client.db

@latteh_app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

from lattechHR import authentication
from lattechHR import dashboard
from lattechHR import calendar_management
from lattechHR import employeeManagement
from lattechHR import emailManagement
from lattechHR import smartDiet
from lattechHR import requests
from lattechHR import company_rules
from lattechHR import evaluation
from lattechHR import asset_manangement




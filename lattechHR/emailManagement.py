from email import message
import smtplib
import ssl
import pymsteams
from flask import Flask, render_template
from lattechHR import latteh_app, DB
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText


def send_email_template(data, template, recipient_email):
    sender_email = latteh_app.config['EMAIL']
    sender_password = latteh_app.config['EMAIL_PASSWORD']
    template_name = f'email_templates/{template}'
    html_content = render_template(template_name, data=data)
    
    try:
        msg = MIMEMultipart()
        msg['From'] = sender_email
        msg['To'] = recipient_email
        msg['Subject'] = data['subject']
        msg.attach(MIMEText(html_content, 'html'))

        server = smtplib.SMTP_SSL(latteh_app.config['SMTP'], 465)
        server.login(sender_email, sender_password)
        server.sendmail(sender_email, recipient_email, msg.as_string())
        server.quit()
    except:
        return {'status': 'error'}
    return {'status': 'success'}
    

def userCredentials(fullname, username, password, receipt_mail):
    data = {'name': fullname, 
            'subject': "Lattech HR - User credentials", 
            'username': username, 
            'password': password}
    status = send_email_template(data, 'welcome.html', receipt_mail)
    return {'message': status}
    

def passwordUpdated(fullname, receipt_mail):
    data = {'name': fullname, 
            'subject': "Lattech HR - Password Updated"}
    status = send_email_template(data, 'password_updated.html', receipt_mail)
    return {'message': status}
    

def resendPassword(fullname, password, receipt_mail):
    data = {'name': fullname,
            'password': password,
            'subject': "Lattech HR - New password"}
    status = send_email_template(data, 'password_resend.html', receipt_mail)
    return {'message': status}
    

def leaveNotification(fullname, start_date, end_date, duration):
    myTeamsMessage = pymsteams.connectorcard(latteh_app.config['TEAMS_WEBHOOK'])
    data = f'{fullname} is taking leave from {start_date} to {end_date}. Duration of this leave is {duration} days.'
    myTeamsMessage.text(data)
    myTeamsMessage.send()
    data = {'name': fullname,
            'start_date': start_date,
            'end_date': end_date,
            'duration': duration,
            'subject': f"Lattech HR - {fullname} leave intimation"}
    for recipient_email in latteh_app.config['ADMIN_EMAIL'].split(','):
        send_email_template(data, 'leave_intimation.html', recipient_email)
import openai
import streamlit as st
import pandas as pd
import base64
import sqlite3
import json
import streamlit.components.v1 as components
from flask import request, send_file, make_response, Response
from lattechHR import latteh_app
from io import BytesIO
from tabulate import tabulate

# Define OpenAI API key
openai.api_key = "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"

# Define client info class for form input
class ClientInfo:
    def __init__(self, full_name, age, weight, weight_unit, height, height_unit, location, dietary_restrictions, health_conditions, activity_level):
        self.full_name = full_name
        self.age = age
        self.weight = weight
        self.weight_unit = weight_unit
        self.height = height
        self.height_unit = height_unit
        self.location = location
        self.dietary_restrictions = dietary_restrictions
        self.health_conditions = health_conditions
        self.activity_level = activity_level

    def __hash__(self):
        return hash((self.full_name, self.age, self.weight, self.weight_unit, self.height, self.height_unit, self.location, self.dietary_restrictions, self.health_conditions, self.activity_level))

    def __eq__(self, other):
        if isinstance(other, ClientInfo):
            return (self.full_name, self.age, self.weight, self.weight_unit, self.height, self.height_unit, self.location, self.dietary_restrictions, self.health_conditions, self.activity_level) == (other.full_name, other.age, other.weight, other.weight_unit, other.height, other.height_unit, other.location, other.dietary_restrictions, other.health_conditions, other.activity_level)
        return False

# Function to generate meal plan using OpenAI GPT-3
def get_meal_plan(_client_info):
    client_info = _client_info

    # Convert weight and height to metric units
    weight = convert_weight_to_kg(client_info.weight, client_info.weight_unit)
    height = convert_height_to_cm(client_info.height, client_info.height_unit)
    
    # Define OpenAI prompt for meal plan generation
    prompt = f"""Generate a personalized meal plan for a client considering the following details:
1. Full Name: {client_info.full_name}
2. Age: {client_info.age}
3. Weight: {weight} kg
4. Height: {height} cm
5. Location: {client_info.location}
6. Dietary Restrictions: {client_info.dietary_restrictions}
7. Health Conditions: {client_info.health_conditions}
8. Activity Level: {client_info.activity_level}

Please provide a 5-day meal plan in a tabled format with detailed information about the food, portion sizes, and nutritional information (including calories, protein, carbohydrates, and fat content). The meal plan should be designed to help the client achieve optimal results based on their personal details and preferences."""

    # Use OpenAI API to generate meal plan text
    response = openai.Completion.create(
        engine="text-davinci-003",
        prompt=prompt,
        max_tokens=500,
        n=1,
        stop=None,
        temperature=0.3,
    )

    return response.choices[0].text.strip()

# Helper function to convert weight to kg
def convert_weight_to_kg(weight, unit):
    if unit == "lbs":
        return weight * 0.453592
    else:
        return weight

# Helper function to convert height to cm
def convert_height_to_cm(height, unit):
    if unit == "ft":
        feet, inches = height.split("'")
        total_inches = int(feet) * 12 + int(inches)
        return total_inches * 2.54
    else:
        return height

# Function to convert meal plan text to Pandas DataFrame
def process_meal_plan(meal_plan_text):
    lines = meal_plan_text.split('\n')
    data = []
    for line in lines:
        if line:
            try:
                row = line.split('|')
                data.append(row)
            except:
                pass

    if len(data) < 2:
        st.warning("The generated meal plan data is not in the expected format. Please try again.")
        return pd.DataFrame()

    data = data[1:]
    df = pd.DataFrame(data, columns=data[0])
    return df

@latteh_app.route('/generate-meal-plan', methods=['GET','POST'])
def generate_meal_plan():
    # client_info = ClientInfo(**request.json)
    # meal_plan_text = get_meal_plan(_client_info=client_info)
    # meal_plan_df = process_meal_plan(meal_plan_text)
    # json_data = meal_plan_df.to_dict()
    json_data = {'Meal ': {0: 'Meal ', 1: '--- ', 2: 'Breakfast ', 3: 'Snack ', 4: 'Lunch ', 5: 'Snack ', 6: 'Dinner ', 7: 'Day 2', 8: 'Meal ', 9: '--- ', 10: 'Breakfast ', 11: 'Snack ', 12: 'Lunch ', 13: 'Snack ', 14: 'Dinner ', 15: 'Day 3', 16: 'Meal ', 17: '--- ', 18: 'Breakfast '}, ' Food ': {0: ' Food ', 1: ' --- ', 2: ' Oatmeal with banana and honey ', 3: ' Greek yogurt with blueberries ', 4: ' Grilled chicken with roasted vegetables ', 5: ' Hummus with carrots and celery ', 6: ' Salmon with quinoa and steamed broccoli ', 7: None, 8: ' Food ', 9: ' --- ', 10: ' Scrambled eggs with bell peppers and spinach ', 11: ' Apple with peanut butter ', 12: ' Turkey sandwich with lettuce and tomato ', 13: ' Greek yogurt with granola ', 14: ' Baked salmon with roasted potatoes and asparagus ', 15: None, 16: ' Food ', 17: ' --- ', 18: ' Smoothie with banana, almond milk, and chia seeds '}, ' Portion Size ': {0: ' Portion Size ', 1: ' --- ', 2: ' 1 cup oatmeal, 1 banana, 1 teaspoon honey ', 3: ' 1 cup Greek yogurt, 1/2 cup blueberries ', 4: ' 2 ounces chicken, 1 cup roasted vegetables ', 5: ' 2 tablespoons hummus, 1/2 cup carrots, 1/2 cup celery ', 6: ' 4 ounces salmon, 1/2 cup quinoa, 1 cup steamed broccoli ', 7: None, 8: ' Portion Size ', 9: ' --- ', 10: ' 2 eggs, 1/2 cup bell peppers, 1/2 cup spinach ', 11: ' 1 apple, 1 tablespoon peanut butter ', 12: ' 2 ounces turkey, 2 slices bread, 1/2 cup lettuce, 1/2 cup tomato ', 13: ' 1 cup Greek yogurt, 1/4 cup granola ', 14: ' 4 ounces salmon, 1/2 cup roasted potatoes, 1 cup asparagus ', 15: None, 16: ' Portion Size ', 17: ' --- ', 18: ' 1 banana, 1 cup almond milk, 1 tablespoon chia'}, ' Calories ': {0: ' Calories ', 1: ' --- ', 2: ' 375 ', 3: ' 200 ', 4: ' 350 ', 5: ' 200 ', 6: ' 500 ', 7: None, 8: ' Calories ', 9: ' --- ', 10: ' 300 ', 11: ' 200 ', 12: ' 350 ', 13: ' 250 ', 14: ' 500 ', 15: None, 16: ' Calories ', 17: ' --- ', 18: None}, ' Protein ': {0: ' Protein ', 1: ' --- ', 2: ' 8 g ', 3: ' 17 g ', 4: ' 28 g ', 5: ' 8 g ', 6: ' 35 g ', 7: None, 8: ' Protein ', 9: ' --- ', 10: ' 18 g ', 11: ' 6 g ', 12: ' 20 g ', 13: ' 17 g ', 14: ' 35 g ', 15: None, 16: ' Protein ', 17: ' --- ', 18: None}, ' Carbohydrates ': {0: ' Carbohydrates ', 1: ' --- ', 2: ' 67 g ', 3: ' 23 g ', 4: ' 25 g ', 5: ' 18 g ', 6: ' 40 g ', 7: None, 8: ' Carbohydrates ', 9: ' --- ', 10: ' 5 g ', 11: ' 25 g ', 12: ' 40 g ', 13: ' 25 g ', 14: ' 40 g ', 15: None, 16: ' Carbohydrates ', 17: ' --- ', 18: None}, ' Fat': {0: ' Fat', 1: ' ---', 2: ' 5 g', 3: ' 5 g', 4: ' 10 g', 5: ' 10 g', 6: ' 20 g', 7: None, 8: ' Fat', 9: ' ---', 10: ' 20 g', 11: ' 10 g', 12: ' 10 g', 13: ' 5 g', 14: ' 20 g', 15: None, 16: ' Fat', 17: ' ---', 18: None}}
    # print(json_data)
    
    df = pd.DataFrame(json_data)
    text_data = df.to_string(index=False)
    response = Response(
        text_data,
        content_type='text/plain',
        headers={'Content-Disposition': 'attachment; filename=data.txt'}
    )
    return response


from unicodedata import category
from lattechHR import latteh_app, DB
from datetime import datetime, timedelta
from flask import render_template, request, session, jsonify
from lattechHR.authentication import check_session_expiration, admin_required
from bson.objectid import ObjectId
from flask_pymongo import DESCENDING
from flask import jsonify, request
from collections import defaultdict
from lattechHR.emailManagement import leaveNotification


@latteh_app.route("/leaves")
@check_session_expiration
def leaves():
    user = DB.users.find_one({'username': session['username']}, {'_id': 0, 'password': 0})
    leaves_data = {'general_leave': user['leaves']['general_leave'], 
                   'paid_leave': user['leaves']['paid_leave'],
                   'festival_leave': user['leaves']['festival_leave'],
                   'lop_leave': user['leaves']['lop_leave']}
    return render_template('calendar/leaves.html', page='leaves', leaves_data=leaves_data)

@latteh_app.route("/calendar")
@check_session_expiration
def calendar():
    return render_template('calendar/calendar.html', page='calendar')

@latteh_app.route("/leaveCalendar")
@check_session_expiration
def leave_calendar():
    return render_template('calendar/calendarSettings.html', page='leaveCalendar')


##############################################
#       Leaves
##############################################

@latteh_app.route("/lop_employees")
@check_session_expiration
@admin_required
def lop_employees():
    return render_template('notification/lop_notification.html', page='LOP Notification')

@latteh_app.route('/api/all_calendar/<username>')
@check_session_expiration
def api_all_calendar(username):
    draw = request.args.get('draw', type=int)
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)

    search_value = request.args.get('search[value]', type=str)

    query = {'username': username, 'category': 'employee_holiday'}

    if search_value:
        query['$or'] = [
            {'event': {'$regex': search_value, '$options': 'i'}},
            {'type': {'$regex': search_value, '$options': 'i'}},
            {'from_date': {'$regex': search_value, '$options': 'i'}},
            {'to_date': {'$regex': search_value, '$options': 'i'}},
            {'duration': {'$regex': search_value, '$options': 'i'}}
        ]

    calendar_data = DB.calendar_events.find(query).sort('to_date', DESCENDING).skip(start).limit(length)

    total_records = DB.calendar_events.count_documents(query)

    output = []
    for data in calendar_data:
        date_obj = datetime.strptime(data['from_date'], '%Y-%m-%d')
        from_date = date_obj.strftime('%d %B, %Y')
        date_obj = datetime.strptime(data['to_date'], '%Y-%m-%d')
        to_date = date_obj.strftime('%d %B, %Y')
        
        current_date_obj = datetime.today()
        past_date = date_obj < current_date_obj
        
        item = {
            'id': str(data['_id']),
            'event': data['event'],
            'type': data['type'].replace('_', ' ').title(),
            'from_date': from_date,
            'to_date': to_date,
            'duration': data['duration'],
            'username': data['username'],
            'past_date': past_date
        }
        output.append(item)

    return jsonify({
        'draw': draw,
        'recordsTotal': total_records,
        'recordsFiltered': total_records,
        'data': output
    })

def calculate_lop(username, month, year):
    lop_count = 0
    for record in DB.calendar_events.find({'type': 'lop_leave'}):
        if (
            record["username"] == username
            and record["type"] == "lop_leave"
            and datetime.strptime(record["from_date"], "%Y-%m-%d").month == month
            and datetime.strptime(record["from_date"], "%Y-%m-%d").year == year
        ):
            lop_count += 1
    return lop_count

@latteh_app.route('/api/api_lop_events')
@check_session_expiration
@admin_required
def api_lop_events():    
    desired_month = int(request.args.get('month'))
    desired_year = int(request.args.get('year'))

    start_date = datetime(desired_year, desired_month, 1)
    end_date = datetime(desired_year, desired_month + 1, 1) if desired_month < 12 else datetime(desired_year + 1, 1, 1)

    records = DB.calendar_events.find({
        'type': 'lop_leave',
        'created_at': {'$gte': start_date, '$lt': end_date}
    })
    data = {}
    for record in records:
        if record["username"] not in data:
            data.update({record["username"]: 1})
        else:
            data[record["username"]] += 1
    record = []
    for key, value in data.items():
        user = DB.users.find_one({'username': key}, {'fullname': 1, 'email': 1, '_id': 0})
        record.append([{'name': user['fullname'], 'email': user['email'], 'username': key, 'lop': value}])

    return jsonify(record)

@latteh_app.route('/calendar', methods=['POST'])
@check_session_expiration
def add_calendar_leave():
    data = request.form.to_dict()
    data['status'] = 'Approved'
    data['username'] = session['username']
    data['category'] = 'employee_holiday'
    user = DB.users.find_one({'username': data['username']}, {'_id': 0, 'password': 0})
    if data['type'] == 'festival_leave':
        data['from_date'] = data['festival_date']
        data['to_date'] = data['festival_date']
    start_date_obj = datetime.strptime(data['from_date'], '%Y-%m-%d')
    end_date_obj = datetime.strptime(data['to_date'], '%Y-%m-%d')
    
    if(data['duration'] == 'half_day'):
        num_days = 0.5
    else:
        delta = end_date_obj - start_date_obj
        num_days = delta.days + 1
    
    num_days = float(num_days)
    if float(user['leaves'][data['type']]) >= num_days:
        data['duration'] = float(num_days)
        record = DB.calendar_events.insert_one(data)
        if data['type'] == 'general_leave':
            DB.users.update_one({'username': data['username']}, {'$inc': {'leaves.general_leave': -num_days}})
        if data['type'] == 'festival_leave':
            DB.users.update_one({'username': data['username']}, {'$inc': {'leaves.festival_leave': -num_days}})
        if data['type'] == 'paid_leave':
            DB.users.update_one({'username': data['username']}, {'$inc': {'leaves.paid_leave': -num_days}})
        # leaveNotification(user['fullname'], start_date_obj.strftime('%d %B %Y'), end_date_obj.strftime('%d %B %Y'), num_days)
    else:
        if data['type'] == 'general_leave':
            if float(user['leaves'][data['type']]) > 0:
                last_date_obj = start_date_obj + timedelta(days=float(user['leaves'][data['type']]) - 1)
                data['to_date'] = last_date_obj.strftime('%Y-%m-%d')
                data['duration'] = float(user['leaves'][data['type']])
                DB.calendar_events.insert_one(data)
                DB.users.update_one({'username': data['username']}, {'$inc': {'leaves.general_leave': -float(user['leaves'][data['type']])}})
                begin_date_obj = last_date_obj + timedelta(days=float(user['leaves'][data['type']]) - 1)
            else:
                begin_date_obj = start_date_obj
                last_date_obj = end_date_obj
                
            lop_leaves =  num_days - float(user['leaves'][data['type']])
            data = {'status': 'Approved', 'username': session['username'], 'category': 'employee_holiday', 'event': data['event'], 'type': 'lop_leave', 'created_at': datetime.now()}
            data['from_date'] = begin_date_obj.strftime('%Y-%m-%d')
            data['to_date'] = end_date_obj.strftime('%Y-%m-%d')
            data['duration'] = lop_leaves
            record = DB.calendar_events.insert_one(data)
            DB.users.update_one({'username': data['username']}, {'$inc': {'leaves.lop_leave': lop_leaves}})
            # leaveNotification(user['fullname'], datetime.strptime(data['from_date'], '%d %B, %Y'), datetime.strptime(data['to_date'], '%d %B, %Y'), num_days)
        else:
            return {
                "message": f"Insufficient number of leaves",
                "status": "error"
            }, 201

    if record:
        return {
            "message": "Leave applied successfully",
            "status": "success"
        }, 201
    else:
        return {
            "message": "Leave couldn't be applied. Please try again.",
            "status": "error"
        }, 201
        


@latteh_app.route("/delete_leave_calendar/<calendar_id>", methods=["DELETE"])
@check_session_expiration
def delete_leave_calendar(calendar_id):
    try:
        if DB.calendar_events.count_documents({'_id': ObjectId(calendar_id)}, limit = 1) != 0:
            calendar_data = DB.calendar_events.find_one({'_id': ObjectId(calendar_id)}, {'_id': 0, 'password': 0})
            date_obj = datetime.strptime(calendar_data['from_date'], '%Y-%m-%d')
            current_date_obj = datetime.today()
            if date_obj > current_date_obj:
                days_diff = calendar_data['duration']
                
                if calendar_data["type"] == 'lop_leave':
                    DB.users.update_one({'username': calendar_data['username']}, {'$inc': {f'leaves.{calendar_data["type"]}': -days_diff}})
                else:
                    DB.users.update_one({'username': calendar_data['username']}, {'$inc': {f'leaves.{calendar_data["type"]}': +days_diff}})

                DB.calendar_events.delete_one({'_id': ObjectId(calendar_id)})
            else:
                return {
                    "message": "Leave record cannot be deleted. Please contact admin.",
                    "status": "error"
                }, 201
            return {
                "message": "Leave record deleted successfully.",
                "status": "success"
            }, 201
        else:
            return {
                "message": "Leave record dosen't exist.",
                "status": "error"
            }, 201
    except Exception as exception:
        return {
                "message": exception,
                "status": "error"
        }, 201


##############################################
#       Company Events
##############################################

@latteh_app.route('/all_events_calendar')
@check_session_expiration
def all_events_calendar():
    calendar_data = DB.calendar_events.find({'category': 'company_holiday'})
    output = []
    for data in calendar_data:
        date_obj = datetime.strptime(data['date'], '%Y-%m-%d').strftime('%d %B, %Y')
        
        item = {
            'event': data['event'],
            'type': data['type'],
            'date': date_obj,
            'category': data['category'],
            'id': str(data['_id'])
        }
        output.append(item)
    return jsonify({'data': output})        

@latteh_app.route('/event_calendar', methods=['POST'])
@check_session_expiration
@admin_required
def event_calendar():
    data = request.form.to_dict()
    data['status'] = 'Approved'
    data['category'] = 'company_holiday'
    data['username'] = session['username']
    record = DB.calendar_events.insert_one(data)
    if record:
        return {
            "message": "Event data added successfully",
            "status": "success"
        }, 201
    else:
        return {
            "message": "Event record couldn't be added. Please try again.",
            "status": "error"
        }, 201
        

@latteh_app.route('/festival_leaves', methods=['GET'])
@check_session_expiration
def festival_leaves():
    calendar_data = DB.calendar_events.find({'category': 'company_holiday', 'type': {'$in': ['Festival Holiday', 'Company Holiday']}})
    output = []
    for data in calendar_data:
        date_obj = datetime.strptime(data['date'], '%Y-%m-%d').strftime('%Y-%m-%d')
        output.append(date_obj)
    return jsonify({'data': output}) 
        

@latteh_app.route("/delete_event_calendar/<calendar_id>", methods=["DELETE"])
@check_session_expiration
@admin_required
def delete_event_calendar(calendar_id):
    try:
        if DB.calendar_events.count_documents({'_id': ObjectId(calendar_id)}, limit = 1) != 0:
            DB.calendar_events.delete_one({'_id': ObjectId(calendar_id)})
            return {
                "message": "User record deleted successfully.",
                "status": "success"
            }, 201
        else:
            return {
                "message": "User record dosen't exist.",
                "status": "error"
            }, 201
    except Exception as exception:
        return {
                "message": exception,
                "status": "error"
        }, 201
    

from lattechHR import latteh_app, DB
from flask import render_template, session, request, jsonify
from datetime import datetime
from bson.objectid import ObjectId
from bson.json_util import dumps
from flask_pymongo import DESCENDING
from lattechHR.authentication import check_session_expiration, admin_required


@latteh_app.route("/requests")
@check_session_expiration
def requests():
    return render_template('employees/requests.html', page='request')

@latteh_app.route("/requestsInfo")
@check_session_expiration
@admin_required
def requestsInfo():
    return render_template('employees/requestsInfo.html', page='request')


@latteh_app.route('/api/requests/<username>')
@check_session_expiration
def api_get_request(username):
    draw = request.args.get('draw', type=int)
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)

    search_value = request.args.get('search[value]', type=str)

    query = {}

    if username != 'all':
        query['username'] = username

    if search_value:
        query['$or'] = [
            {'note': {'$regex': search_value, '$options': 'i'}},
            {'type': {'$regex': search_value, '$options': 'i'}},
            {'date': {'$regex': search_value, '$options': 'i'}},
            {'name': {'$regex': search_value, '$options': 'i'}}
        ]

    requests_data = DB.requests.find(query).sort('date', DESCENDING).skip(start).limit(length)
    total_records = DB.requests.count_documents(query)

    output = []
    for req in requests_data:
        req['id'] = str(req['_id'])
        req['name'] = DB.users.find_one({"username": req['username']})['fullname']
        del req['_id']
        output.append(req)

    return jsonify({
        'draw': draw,
        'recordsTotal': total_records,
        'recordsFiltered': total_records,
        'data': output
    })



@latteh_app.route('/requests', methods=['POST'])
@check_session_expiration
def add_request():
    data = request.form.to_dict()
    data['username'] = session['username']
    data['date'] = datetime.now().strftime('%d %B, %Y')
    record = DB.requests.insert_one(data)
    if record:
        return {
            "message": "Request data added successfully",
            "status": "success"
        }, 201
    else:
        return {
            "message": "Request record couldn't be added. Please try again.",
            "status": "error"
        }, 201
        

@latteh_app.route("/delete_request/<request_id>", methods=["DELETE"])
@check_session_expiration
def delete_request(request_id):
    try:
        if DB.requests.count_documents({'_id': ObjectId(request_id)}, limit = 1) != 0:
            DB.requests.delete_one({'_id': ObjectId(request_id)})
            return {
                "message": "Request record deleted successfully.",
                "status": "success"
            }, 201
        else:
            return {
                "message": "Request record dosen't exist.",
                "status": "error"
            }, 201
    except Exception as exception:
        return {
                "message": exception,
                "status": "error"
        }, 201
    
        
    


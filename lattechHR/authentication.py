from email import message
import smtplib
import ssl
import uuid
import pyotp
from bson import re
from lattechHR import latteh_app, DB
from datetime import datetime, timezone
from flask import request, jsonify, render_template, session, redirect
from passlib.hash import sha256_crypt
from lattechHR.emailManagement import passwordUpdated, resendPassword


def check_session_expiration(func):
    def wrapper(*args, **kwargs):
        if 'username' not in session:
            return redirect("/")
        elif 'last_activity' in session and datetime.now(timezone.utc) - session['last_activity'] > latteh_app.permanent_session_lifetime:
            session.pop('username', None)
            session.pop('last_activity', None)
            return redirect("/")
        session['last_activity'] = datetime.now()
        return func(*args, **kwargs)
    wrapper.__name__ = func.__name__
    return wrapper

def admin_required(func):
    def wrapper(*args, **kwargs):
        if 'role' not in session:
            return redirect("/dashboard")
        elif session['role'] != 'admin':
            return redirect("/dashboard")
        return func(*args, **kwargs)
    wrapper.__name__ = func.__name__
    return wrapper 

def moderator_required(func):
    def wrapper(*args, **kwargs):
        print(f"session['role'] {session['role']}")
        if 'role' not in session:
            return redirect("/dashboard")
        if session['role'] == 'moderator' or session['role'] == 'admin':
            return func(*args, **kwargs)
        else:
            return redirect("/dashboard")
    wrapper.__name__ = func.__name__
    return wrapper  


@latteh_app.route("/", methods=["GET", "POST"])
def login():
    message = ''
    if request.method == "POST":
        username = request.form.get("username")
        password = request.form.get("password")
        if DB.users.count_documents({'username': username}, limit = 1) != 0 and DB.users.find_one({'username': username}).get("status") != 'deactive':
            user_data = DB.users.find_one({'username': username},sort=[('_id', -1 )])
            if sha256_crypt.verify(password, user_data['password']):
                session.permanent = True
                session['username'] = username
                session['fullname'] = user_data['fullname']
                session['designation'] = user_data['designation']
                session['role'] = user_data['role']
                session['last_activity'] = datetime.now()
                return redirect("/dashboard")
        message = 'Please enter correct credentials.'    
    return render_template("authentication/login.html", message=message)


@latteh_app.route("/reset_password", methods=["GET", "POST"])
def reset_password():
    record = DB.users.update_one({'username': session.get('username')}, {'$set': {'password': sha256_crypt.encrypt(request.form.get("password"))}})
    if record:
        return {
            "message": "Password updated successfully",
            "status": "success"
        }, 201
    else:
        return {
            "message": "Password couldn't be updated. Please try again.",
            "status": "error"
        }, 201
        

@latteh_app.route("/reset_password/<username>/<email>/<fullname>", methods=["POST"])
@admin_required
def resetPassword(username, email, fullname):
    record = DB.users.update_one({'username': username}, {'$set': {'password': sha256_crypt.encrypt(request.form.get("password"))}})
    resendPassword(fullname, request.form.get("password"), email)
    if record:
        return {
            "message": "Password updated successfully",
            "status": "success"
        }, 201
    else:
        return {
            "message": "Password couldn't be updated. Please try again.",
            "status": "error"
        }, 201


@latteh_app.route("/forgot_password", methods=["GET", "POST"])
def forgot_password():
    message = {'status': 'none'}
    if request.method == "POST":
        message = {'status': 'error'}
        username = request.form.get("username")
        otp = request.form.get("otp")
        user_data = DB.users.find_one({'username': username},sort=[('_id', -1 )])
        totp = pyotp.TOTP(user_data['qr_code'])
        if totp.verify(otp):
            session.permanent = True
            session['username'] = username
            session['fullname'] = user_data['fullname']
            session['designation'] = user_data['designation']
            session['role'] = user_data['role']
            session['image'] = user_data['image']
            session['last_activity'] = datetime.now()
            message = {'status': 'success'}
            passwordUpdated(user_data['fullname'], user_data['email'])
            return redirect("/dashboard")     
    return render_template("authentication/forgotPassword.html", message=message)


@latteh_app.route("/logout")
@check_session_expiration
def logout():
    session.pop('username', None)
    session.pop('last_activity', None)
    return redirect("/")
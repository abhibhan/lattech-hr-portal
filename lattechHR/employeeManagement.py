from distutils.command.config import config
from itertools import count
import qrcode
import pdfkit
import os
import pyotp
import openai
import pytz
from io import BytesIO
from bson import re
from lattechHR import latteh_app, DB
from flask import request, jsonify, render_template, session, send_file
from passlib.hash import sha256_crypt
from lattechHR.authentication import check_session_expiration, admin_required, moderator_required
from lattechHR.emailManagement import userCredentials, send_email_template
from datetime import datetime
from flask_pymongo import DESCENDING

# Todo:
# 1) Calculate lop days for a particular user in current month


@latteh_app.route("/employees")
@check_session_expiration
@admin_required
def employees():
    user_data = DB.users.find({})
    output = []
    for record in user_data:
        record['_id'] = str(record['_id'])
        output.append(record)
    return render_template('employees/employees.html', user_data=output, page='employee')


@latteh_app.route("/api/employees")
@check_session_expiration
@admin_required
def api_employees():
    records = DB.users.find({'status': 'active'}, {
                            '_id': 0, 'fullname': 1, 'username': 1})
    return {
        "data": list(records),
        "status": "success"
    }, 201


@latteh_app.route("/update_password", methods=['POST'])
@check_session_expiration
def update_password():
    data = request.form.to_dict()
    data['password'] = sha256_crypt.encrypt(data['password'])
    record = DB.users.update_one(
        {'username': session['username']}, {'$set': data})
    if record:
        return {
            "message": "Passowrd updated successfully",
            "status": "success"
        }, 201
    else:
        return {
            "message": "Password couldn't be updated. Please try again.",
            "status": "error"
        }, 201


@latteh_app.route('/all_employees/<status>')
@check_session_expiration
@admin_required
def all_employees(status):
    employee_data = DB.users.find({"$or": [
        {"status": status},
        {"status": {"$exists": False}}
    ]})
    data = []
    for employee in employee_data:
        item = {
            'fullname': employee['fullname'],
            'email': employee['email'],
            'phone': employee['phone'],
            'employee_id': employee['employee_id'],
            'designation': employee['designation'],
            'username': employee['username'],
            'status': 'active'
        }
        if 'status' in employee:
            item['status'] = employee['status']
        data.append(item)
    return jsonify({'data': data})


def check_username(username):
    user = DB.users.find_one({"username": username})
    if user:
        return True
    else:
        return False


def check_email(email):
    user = DB.users.find_one({"email": email})
    if user:
        return True
    else:
        return False


@latteh_app.route('/employees', methods=['POST'])
@check_session_expiration
@admin_required
def add_employee():
    data = request.form.to_dict()
    if not check_username(data['username']) and not check_email(data['email']):
        email_password = data['password']
        data['password'] = sha256_crypt.encrypt(data['password'])
        data['paid_leave'] = 0
        data['leaves'] = {'general_leave': float(data['general_leave']), 'festival_leave': float(
            data['festival_leave']), 'paid_leave': float(data['paid_leave']), 'lop_leave': float(data['lop_leave']),
            'wfh_leave': float(data['wfh_leave'])}
        del data['general_leave']
        del data['festival_leave']
        del data['paid_leave']
        del data['lop_leave']
        del data['wfh_leave']
        image = request.files["profile_image"]
        if image:
            image.save(os.path.join(
                latteh_app.config['USER_PROFILE_PHOTO'], image.filename))
            data['image'] = image.filename
        else:
            data['image'] = ''
        record = DB.users.insert_one(data)
        generate_qr(data['username'])
        if record:
            userCredentials(data['fullname'], data['username'],
                            email_password, data['email'])
            return {
                "message": "User data added successfully",
                "status": "success"
            }, 201
    else:
        return {
            "message": "User record couldn't be added. Please try again.",
            "status": "error"
        }, 201


@latteh_app.route('/employees/<username>', methods=['GET'])
@check_session_expiration
@admin_required
def get_employee_info(username):
    user = DB.users.find_one({'username': username}, {'_id': 0, 'password': 0})
    if user is None:
        return jsonify({}), 201
    return jsonify(user), 201


@latteh_app.route('/employees/<username>', methods=['PUT'])
@check_session_expiration
@admin_required
def update_employee(username):
    new_employee = request.form.to_dict()
    new_employee['leaves'] = {'general_leave': float(new_employee['general_leave']), 'festival_leave': float(
        new_employee['festival_leave']), 'paid_leave': float(new_employee['paid_leave']), 'lop_leave': float(new_employee['lop_leave']),
        'wfh_leave': float(new_employee['wfh_leave'])}
    del new_employee['general_leave']
    del new_employee['festival_leave']
    del new_employee['paid_leave']
    del new_employee['lop_leave']
    del new_employee['wfh_leave']
    result = DB.users.update_one(
        {'username': username}, {'$set': new_employee})

    if result.modified_count == 1:
        return jsonify({'message': 'Employee updated successfully', "status": "success"})
    else:
        return jsonify({'message': 'Employee not found', "status": "error"})


@latteh_app.route("/employees/<username>", methods=["DELETE"])
@check_session_expiration
@admin_required
def delete_employee(username):
    try:
        if DB.users.count_documents({'username': username}, limit=1) != 0:
            DB.users.delete_one({'username': username})
            return {
                "message": "User record deleted successfully.",
                "status": "success"
            }, 201
        else:
            return {
                "message": "User record dosen't exist.",
                "status": "error"
            }, 201
    except Exception as exception:
        return {
            "message": exception,
            "status": "error"
        }, 201


@latteh_app.route("/set_employee_status/<username>/<status>", methods=["GET"])
@check_session_expiration
@admin_required
def set_employee_status(username, status):
    try:
        if DB.users.count_documents({'username': username}, limit=1) != 0:
            DB.users.update_one({'username': username}, {
                                '$set': {'status': status}}, upsert=True)
            return {
                "message": "User status updated successfully.",
                "status": "success"
            }, 201
        else:
            return {
                "message": "User record dosen't exist.",
                "status": "error"
            }, 201
    except Exception as exception:
        return {
            "message": exception,
            "status": "error"
        }, 400


@latteh_app.route("/corn_leaves/<leave_duration>", methods=["GET"])
@check_session_expiration
@admin_required
def cornMonthlyLeaves(leave_duration):
    leave_duration = latteh_app.config['GENERAL_LEAVE_PER_MONTH']
    wfh_leave_duration = latteh_app.config['WORK_FROM_HOME_PER_MONTH']
    for data in DB.users.find({}, {"username": 1}):
        try:
            if DB.users.count_documents({'username': data['username']}, limit=1) != 0:
                DB.users.update_one({'username': data['username']}, {'$inc': {'leaves.general_leave': float(
                    leave_duration), 'leaves.wfh_leave': float(wfh_leave_duration)}})
        except:
            pass
    current_date = datetime.now()
    DB.notification.insert_one(
        {'data': f'General Leave {leave_duration} days and WFH of {wfh_leave_duration} days added for {current_date.strftime("%B, %Y")}', 'date': current_date, 'type': 'leave'})
    return {
        "message": "Leaves allotted successfully.",
        "status": "success"
    }, 201


@latteh_app.route("/transfer_to_paid_leaves", methods=["GET"])
@check_session_expiration
@admin_required
def transfer_to_paid_leaves():
    for data in DB.users.find({}, {"username": 1}):
        try:
            if DB.users.count_documents({'username': data['username']}, limit=1) != 0:
                general_leaves = DB.users.find_one({"username": data['username']}, {
                                                   "leaves.general_leave": 1, "_id": 0}).get("leaves", {}).get("general_leave", 0)
                DB.users.update_one({'username': data['username']},
                                    {
                    '$inc': {
                        'leaves.paid_leave': float(general_leaves)
                    },
                    '$set': {
                        'leaves.general_leave': 0.0
                    }
                })
        except:
            pass
    current_date = datetime.now()
    DB.notification.insert_one(
        {'data': f'Paid leaves transferred for {current_date.strftime("%B")}, {current_date.strftime("%Y")} ', 'date': current_date, 'type': 'Paid leave'})

    return {
        "message": "Genral leaves transferred to Paid leaves successfully.",
        "status": "success"
    }, 201

###############################################
#   Profile Page
###############################################


@latteh_app.route("/employee_profile")
@check_session_expiration
def employee_profile():
    user_data = DB.users.find({'username': session['username']})
    output = []
    for record in user_data:
        record['_id'] = str(record['_id'])
        output.append(record)
    return render_template('user_management/user-profile.html', user_data=output, page='employee_profile')


@latteh_app.route("/profile")
@check_session_expiration
def profile():
    user = DB.users.find_one({'username': session['username']}, {
                             '_id': 0, 'password': 0})
    return render_template('employees/profile.html', user_data=user, page='profile')


@latteh_app.route("/generate_qr/<username>")
def generate_qr(username):
    secret_key = pyotp.random_base32()
    DB.users.update_one({'username': username}, {
                        '$set': {'qr_code': secret_key}})
    totp = pyotp.TOTP(secret_key)
    otp_url = totp.provisioning_uri(f"Lattech HR ({username})")
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_H,
        box_size=10,
        border=4,
    )
    qr.add_data(otp_url)
    qr.make(fit=True)
    img = qr.make_image()
    img.save(os.path.join(latteh_app.root_path, 'static',
             'images', 'qrcode', f"{username}.jpg"))
    return {'message': 'success'}


@latteh_app.route("/detailed_profile/<username>")
@check_session_expiration
@admin_required
def detailedprofile(username):
    user = DB.users.find_one({'username': username}, {'_id': 0, 'password': 0})
    return render_template('employees/detailedProfile.html', user_data=user, page='profile')


###############################################
#   Attendance Page
###############################################

@latteh_app.route("/attendance_page")
@check_session_expiration
def attendancePage():
    return render_template('employees/attendance.html', page='attendance')


@latteh_app.route("/logged_employee_page")
@check_session_expiration
@moderator_required
def logged_employee_page():
    current_date = datetime.now().strftime("%Y-%m-%d")
    pipeline = [
        {
            '$match': {'status': 'active'}
        },
        {
            '$lookup': {
                'from': 'attendance',
                'let': {'username': '$username'},
                'pipeline': [
                    {
                        '$match': {
                            '$expr': {
                                '$and': [
                                    {'$eq': ['$username', '$$username']},
                                    {'$eq': ['$currentdate', current_date]}
                                ]
                            }
                        }
                    }
                ],
                'as': 'attendance_data'
            }
        },
        {
            '$unwind': {
                'path': '$attendance_data',
                'preserveNullAndEmptyArrays': True
            }
        },
        {
            '$project': {
                '_id': 0,
                'username': 1,
                'fullname': 1,
                'signin': {
                    '$ifNull': ['$attendance_data.signin', 'NA']
                },
                'signout': {
                    '$ifNull': ['$attendance_data.signout', 'NA']
                },
                'worklocation': {
                    '$ifNull': ['$attendance_data.worklocation', 'NA']
                }
            }
        }
    ]

    # Execute the aggregation pipeline and retrieve the result
    result = list(DB.users.aggregate(pipeline))
    return render_template('employees/loggedEmployeeList.html', page='Logged Employee', records=result)


@latteh_app.route("/attendance/<username>/<login_type>/<worklocation>", methods=["GET"])
@check_session_expiration
def attendance(username, login_type, worklocation):
    current_date = datetime.now().strftime("%Y-%m-%d")
    record = DB.attendance.find_one(
        {'username': username, 'currentdate': current_date})

    if record:
        if login_type == 'signout':
            now = datetime.now()
            ist = pytz.timezone('Asia/Kolkata')
            ist_time = now.astimezone(ist)
            time_str = ist_time.strftime('%I:%M %p')
            DB.attendance.update_one({'_id': record['_id']}, {
                                     '$set': {'signout': time_str}})
            return {
                "message": 'Sign out updated',
                "status": "success"
            }, 201
        else:
            return {
                "message": 'Wrong entry',
                "status": "error"
            }, 201
    else:
        if login_type == 'signin':
            now = datetime.now()
            ist = pytz.timezone('Asia/Kolkata')
            ist_time = now.astimezone(ist)
            time_str = ist_time.strftime('%I:%M %p')
            # if worklocation == 'home':
            #     val = DB.users.find_one({"username": username}, {"leaves.wfh_leave": 1, "_id": 0}).get("leaves", {}).get("wfh_leave", 0)
            #     print(f'WFH {type(val)} - \{val}/ {val>0.0}')
            #     if DB.users.find_one({"username": username}, {"leaves.wfh_leave": 1, "_id": 0}).get("leaves", {}).get("wfh_leave", 0) > 0:
            #         DB.users.update_one({'username': username}, {
            #                             '$inc': {'leaves.wfh_leave': -1}})
            #     else:
            #         DB.users.update_one({'username': username}, {
            #                             '$inc': {'leaves.lop_leave': 1}})
            #         data = {'status': 'Approved', 'username': username,
            #                 'category': 'employee_holiday', 'event': 'No WFH Credits', 'type': 'lop_leave'}
            #         data['from_date'] = current_date
            #         data['to_date'] = current_date
            #         data['created_at'] = datetime.now()
            #         data['duration'] = 1
            #         DB.calendar_events.insert_one(data)
            attendance_record = {'username': username,
                                 'signin': time_str,
                                 'currentdate': current_date,
                                 'created_at': datetime.now(),
                                 'worklocation': worklocation}
            DB.attendance.insert_one(attendance_record)
            return {
                "message": 'Sign in updated',
                "status": "success"
            }, 201
        else:
            return {
                "message": 'Wrong entry',
                "status": "error"
            }, 201


@latteh_app.route("/api/attendance_info/<username>")
@check_session_expiration
def api_attendance_info(username):
    draw = request.args.get('draw', type=int)
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)
    total_records = DB.attendance.count_documents({'username': username})

    attendance_info = DB.attendance.find({'username': username}, {
        '_id': 0, 'username': 0
    }).sort('currentdate', DESCENDING).skip(start).limit(length)

    data = []
    for attendance_data in attendance_info:
        if 'signin' not in attendance_data:
            attendance_data['signin'] = ''
        if 'signout' not in attendance_data:
            attendance_data['signout'] = ''
        data.append(attendance_data)

    return jsonify({
        'draw': draw,
        'recordsTotal': total_records,
        'recordsFiltered': total_records,
        'data': data
    })


@latteh_app.route('/api/wfh_attendance/<username>')
@check_session_expiration
def api_wfh_attendance(username):
    draw = request.args.get('draw', type=int)
    start = request.args.get('start', type=int)
    length = request.args.get('length', type=int)

    search_value = request.args.get('search[value]', type=str)

    query = {'username': username, 'worklocation': 'home'}

    if search_value:
        query['$or'] = [
            {'currentdate': {'$regex': search_value, '$options': 'i'}},
            {'signin': {'$regex': search_value, '$options': 'i'}},
            {'signout': {'$regex': search_value, '$options': 'i'}}
        ]

    attendance_data = DB.attendance.find(query).sort(
        'currentdate', DESCENDING).skip(start).limit(length)

    total_records = DB.attendance.count_documents(query)

    output = []
    for data in attendance_data:
        date_obj = datetime.strptime(data['currentdate'], '%Y-%m-%d')
        current_date = date_obj.strftime('%d %B, %Y')

        signout = '---'
        if 'signout' in data:
            signout = data['signout']

        item = {
            'date': current_date,
            'signin': data['signin'],
            'signout': signout
        }
        output.append(item)

    return jsonify({
        'draw': draw,
        'recordsTotal': total_records,
        'recordsFiltered': total_records,
        'data': output
    })

###############################################
#   Salary Slip
###############################################


@latteh_app.route('/add_salary_component/<username>', methods=['POST'])
@check_session_expiration
@admin_required
def addSalaryComponent(username):
    data = request.form.to_dict()
    record = DB.users.update_one({'username': username}, {
                                 '$set': {f'salary.{data["type"]}.{data["component"]}': float(data["amount"])}})
    if record:
        return {
            "message": "Salary data added successfully",
            "status": "success"
        }, 201
    else:
        return {
            "message": "Salary record couldn't be added. Please try again.",
            "status": "error"
        }, 201


@latteh_app.route('/get_salary_component/<username>', methods=['GET'])
@check_session_expiration
@admin_required
def getSalaryComponent(username):
    output = []
    try:
        user = DB.users.find_one({'username': username})
        if not user:
            return {
                "message": "Can't fetch salary component data",
                "status": "error"
            }, 201

        if 'earnings' in user['salary']:
            earnings = user['salary']['earnings']
            for key, val in earnings.items():
                output.append(
                    {'component': key, 'type': 'earnings', 'amount': val})

        if 'deductions' in user['salary']:
            deductions = user['salary']['deductions']
            for key, val in deductions.items():
                output.append(
                    {'component': key, 'type': 'deductions', 'amount': val})
    except:
        pass

    return jsonify({'data': output})


@latteh_app.route("/delete_salary_component/<username>/<component>/<type>", methods=["DELETE"])
@check_session_expiration
@admin_required
def deleteSalaryComponent(username, component, type):
    try:
        DB.users.update_one({'username': username}, {
                            '$unset': {f'salary.{type}.{component}': ""}})
        return {
            "message": "Salary record deleted successfully.",
            "status": "success"
        }, 201
    except Exception as exception:
        return {
            "message": exception,
            "status": "error"
        }, 201


@latteh_app.route('/payslip_months/<username>', methods=['GET'])
@check_session_expiration
def paySlipMonths(username):
    payslip_data = DB.payslip.find({'username': username})
    output = []
    for data in payslip_data:
        output.append(data['period'])
    return jsonify({'data': output})


@latteh_app.route('/corn_generate_payslip', methods=['GET'])
@check_session_expiration
@admin_required
def cornGeneratePaySlip():
    # Todo: Calculate lop days for a particular user in current month
    for data in DB.users.find({}, {"username": 1}):
        try:
            user = DB.users.find_one({'username': data['username']})
            now = datetime.now()
            if DB.payslip.count_documents({'username': data['username'], 'period': f'{now.strftime("%B")} {now.year}'}) == 0:
                DB.payslip.insert_one({
                    'username': user['username'],
                    'fullname': user['fullname'],
                    'employee_id': user['employee_id'],
                    'adhar': user['adhar'],
                    'pan': user['pan'],
                    'bank_name': user['bank_name'],
                    'account_number': user['account_number'],
                    'doj': user['doj'],
                    'designation': user['designation'],
                    'department': user['department'],
                    'salary': user['salary'],
                    'period': f'{now.strftime("%B")} {now.year}',
                    'lop': 0
                })
        except:
            pass
    return {
        "message": f"Generate Payslip for {now.strftime('%B')} {now.year}",
        "status": "success"
    }, 201


@latteh_app.route('/download_payslip', methods=['POST'])
@check_session_expiration
def downloadPaySlip():
    period = request.form['slip']
    username = session['username']
    if DB.payslip.count_documents({'username': username, 'period': period}) == 1:
        payslip_data = DB.payslip.find_one(
            {'username': username, 'period': period})

        total_earnings = sum(
            [float(v) for v in payslip_data["salary"]["earnings"].values()])
        total_deductions = sum(
            [float(v) for v in payslip_data["salary"]["deductions"].values()])
        net_pay = total_earnings - total_deductions

        earnings_rows = ""
        for key, value in payslip_data["salary"]["earnings"].items():
            earnings_rows += f"<tr><td>{key}</td><td>&#8377; {value}</td></tr>"
        earnings_rows += f"<tr><td><b>Total</b></td><td>&#8377; {total_earnings}</td></tr>"

        deductions_rows = ""
        for key, value in payslip_data["salary"]["deductions"].items():
            deductions_rows += f"<tr><td>{key}</td><td>&#8377; {value}</td></tr>"
        deductions_rows += f"<tr><td><b>Total</b></td><td>&#8377; {total_deductions}</td></tr>"

        payslip_content = '''
        <!DOCTYPE html>
        <html>
        <head>
            <style>
                body {
                    font-family: Arial, sans-serif;
                    margin: 0;
                    padding: 0;
                    background-color: #fff;
                }
                .container {
                    max-width: 800px;
                    margin: 0 auto;
                    padding: 20px;
                    background-color: #fff;
                    box-shadow: 0 0 10px rgba(0,0,0,0.2);
                }
                h1 {
                    font-size: 24px;
                    font-weight: bold;
                    margin-top: 0;
                    margin-bottom: 20px;
                    color: #0077b5;
                    text-align: center;
                    text-transform: uppercase;
                }
                table {
                    width: 100%;
                    border-collapse: collapse;
                    margin-bottom: 20px;
                }
                th, td {
                    padding: 10px;
                    text-align: left;
                    border-bottom: 1px solid #ddd;
                }
                th {
                    font-weight: bold;
                    background-color: #f1f1f1;
                }
                td.salary {
                    font-weight: bold;
                    font-size: 18px;
                    color: #0077b5;
                }
                footer {
                    font-size: 12px;
                    color: #666;
                    text-align: center;
                    margin-top: 40px;
                    padding-top: 10px;
                    border-top: 1px solid #ddd;
                }
            </style>
        </head>
        <body>
            <div class="container">
                <h1 style="margin-bottom: 0px;">Elegocart Technology Private Limited</h1>
                <h1 style="color: black;font-size: 14px;">KLE Tech Park, Vidyanagar, Hubli - 580031</h1>
                <table>
                    <tr>
                        <th>Employee Name:</th>
                        <td>'''+payslip_data['fullname']+'''</td>
                        <th>Aadhar Number:</th>
                        <td>'''+payslip_data['adhar']+'''</td>
                    </tr>
                    <tr>
                        <th>Employee ID:</th>
                        <td>'''+payslip_data['employee_id']+'''</td>
                        <th>PAN Card Number:</th>
                        <td>'''+payslip_data['pan']+'''</td>
                    </tr>
                    <tr>
                        <th>Designation:</th>
                        <td>'''+payslip_data['designation']+'''</td>
                        <th>Bank Name:</th>
                        <td>'''+payslip_data['bank_name']+'''</td>
                    </tr>
                    <tr>
                        <th>Department:</th>
                        <td>'''+payslip_data['department']+'''</td>
                        <th>Bank Account No:</th>
                        <td>'''+payslip_data['account_number']+'''</td>
                    </tr>
                    <tr>
                        <th>Joining Date:</th>
                        <td>'''+payslip_data['doj']+'''</td>
                        <th>Paying days:</th>
                        <td>30</td>
                    </tr>
                    <tr>
                        <th>Paying Period:</th>
                        <td>'''+period+'''</td>
                        <th>LOP days:</th>
                        <td>'''+str(payslip_data['lop'])+'''</td>
                        
                    </tr>
                </table>
                <table>
                    <tr>
                        <th>Earnings</th>
                        <th>Amount (INR)</th>
                    </tr>
                    '''+earnings_rows+'''
                    <tr>
                        <th>Deductions</th>
                        <th>Amount (INR)</th>
                    </tr>
                    '''+deductions_rows+'''
                    <br>
                    <tr>
                        <th>Net Pay (INR)</th>
                        <th>&#8377; '''+str(net_pay)+'''</th>
                    </tr>
                </table>
                <br>
                <table>
                    <tr>
                        <td>Elegocart Technology Private Limited</td>
                        <td></td>
                        <td>Authorised Signatory:</td>
                        <td></td>
                    </tr>
                </table>
            </div>
        </body>
        '''
        output_file_path = f'{latteh_app.config["PAYSLIP_DIRECTORY"]}/{username}_{period}.pdf'
        options = {
            'page-size': 'Letter',
            'margin-top': '0mm',
            'margin-right': '0mm',
            'margin-bottom': '0mm',
            'margin-left': '0mm',
        }

        pdfkit.from_string(payslip_content, output_file_path, options=options)
        return send_file(output_file_path, as_attachment=True)
    else:
        return {
            "message": "Payslip in unavailable.",
            "status": "error"
        }, 201

###############################################
#   Notifications
###############################################


@latteh_app.route("/general_notification")
@check_session_expiration
@admin_required
def general_notification():
    user_data = DB.notification.find().sort([("date", 1)])
    output = []
    for record in user_data:
        record['_id'] = str(record['_id'])
        output.append(record)
    return render_template('notification/notification.html', user_data=output, page='notification')

###############################################
#   Chat GPT
###############################################


@latteh_app.route('/askgpt/<question>', methods=['GET'])
@check_session_expiration
@admin_required
def askGPT(question):
    model_engine = "text-davinci-002"
    prompt = f"Q: {question}\nA:"
    response = openai.Completion.create(
        engine=model_engine,
        prompt=prompt,
        max_tokens=1024,
        n=1,
        stop=None,
        temperature=0.7,
    )
    answer = response.choices[0].text.strip()
    return {'data': answer}


###############################################
#   General Settings
###############################################

@latteh_app.route('/general_settings', methods=['GET'])
@check_session_expiration
@admin_required
def general_settings():
    data = DB.setting.find_one({}, {'_id': 0})
    return render_template('employees/general_setting.html', page='general setting', data=data)


@latteh_app.route('/toggle_evaluation', methods=['GET'])
@check_session_expiration
@admin_required
def toggle_evaluation():
    try:
        setting = DB.setting.find_one()
        current_value = request.args.get('evaluation', False)
        if setting:
            DB.setting.update_one({}, {'$set': {'evaluation': current_value}})
        else:
            DB.setting.insert_one({'evaluation': current_value})
        return jsonify({'status': 'success', 'message': f'Evaluation is now {current_value}'})
    except Exception as e:
        return jsonify({'status': 'error', 'message': str(e)})

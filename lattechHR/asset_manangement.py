from lattechHR import latteh_app, DB
from flask_pymongo import DESCENDING
from lattechHR.authentication import check_session_expiration, admin_required
from flask import request, jsonify, render_template, session, send_file


@latteh_app.route("/asset_settings")
@check_session_expiration
def asset_settings():
    data = DB.setting.find_one({}, {'_id': 0})
    category = []
    status = []
    if 'asset_category' in data:
        category = data['asset_category']
    if 'asset_status' in data:
        status = data['asset_status']
    return render_template('assets/assetSettings.html', page='Asset Category', category=category, status=status)


@latteh_app.route("/api/asset_category", methods=["POST", "DELETE"])
def asset_category():
    if request.method == 'POST':
        try:
            data = request.form.to_dict()
            update_query = {
                "$addToSet": {"asset_category": {"$each": [data['category']]}}
            }
            DB.setting.update_one({}, update_query, upsert=True)
            return {
                "status": "success",
                "data": []
            }, 201
        except Exception as exception:
            return {
                "message": exception,
                "status": "error"
            }, 400
    if request.method == 'DELETE':
        try:
            data = request.form.to_dict()
            update_query = {
                "$pull": {"asset_category": data['category']}
            }
            DB.setting.update_one({}, update_query, upsert=True)
            return {
                "status": "success",
                "message": "Record deleted successfully"
            }, 201
        except Exception as exception:
            return {
                "message": exception,
                "status": "error"
            }, 400


@latteh_app.route("/api/asset_status", methods=["POST", "DELETE"])
def asset_status():
    if request.method == 'POST':
        try:
            data = request.form.to_dict()
            update_query = {
                "$addToSet": {"asset_status": {"$each": [data['status_data']]}}
            }
            DB.setting.update_one({}, update_query, upsert=True)
            return {
                "status": "success",
                "data": []
            }, 201
        except Exception as exception:
            return {
                "message": exception,
                "status": "error"
            }, 400
    if request.method == 'DELETE':
        try:
            data = request.form.to_dict()
            update_query = {
                "$pull": {"asset_status": data['status']}
            }
            DB.setting.update_one({}, update_query, upsert=True)
            return {
                "status": "success",
                "message": "Record deleted successfully"
            }, 201
        except Exception as exception:
            return {
                "message": exception,
                "status": "error"
            }, 400


@latteh_app.route("/company_assets")
@check_session_expiration
def company_assets():
    data = DB.setting.find_one({}, {'_id': 0})
    category = []
    status = []
    if 'asset_category' in data:
        category = data['asset_category']
    if 'asset_status' in data:
        status = data['asset_status']
    return render_template('assets/companyAssets.html', page='Company Asset', category=category, status=status)


@latteh_app.route("/api/company_assets", methods=["GET", "POST", "DELETE"])
def company_asset_records():
    if request.method == 'GET':
        draw = request.args.get('draw', type=int)
        start = request.args.get('start', type=int)
        length = request.args.get('length', type=int)
        asset_id = request.args.get('asset_id', '')

        if asset_id == '':
            search_value = request.args.get('search[value]', type=str)
            query = {}

            if search_value:
                query['$or'] = [
                    {'title': {'$regex': search_value, '$options': 'i'}},
                    {'asset_tag': {'$regex': search_value, '$options': 'i'}},
                    {'asset_status': {'$regex': search_value, '$options': 'i'}},
                    {'specs': {'$regex': search_value, '$options': 'i'}}
                ]

            requests_data = DB.company_assets.find(query).sort(
                'date', DESCENDING).skip(start).limit(length)
            total_records = DB.company_assets.count_documents(query)

            output = []
            for req in requests_data:
                del req['_id']
                output.append(req)

            return jsonify({
                'draw': draw,
                'recordsTotal': total_records,
                'recordsFiltered': total_records,
                'data': output
            })
        else:
            asset_id = request.args.get('asset_id', '')
            requests_data = DB.company_assets.find(
                {'asset_tag': asset_id}, {'_id': 0})
            return {
                "message": list(requests_data),
                "status": "success"
            }, 201
    if request.method == 'POST':
        try:
            data = request.form.to_dict()
            DB.company_assets.insert_one(data)
            return {
                "status": "success",
                "data": "Asset record added successfully"
            }, 201
        except Exception as exception:
            return {
                "message": exception,
                "status": "error"
            }, 400
    if request.method == 'DELETE':
        try:
            data = request.form.to_dict()
            update_query = {
                "$pull": {"asset_status": data['status']}
            }
            DB.setting.update_one({}, update_query, upsert=True)
            return {
                "status": "success",
                "message": "Record deleted successfully"
            }, 201
        except Exception as exception:
            return {
                "message": exception,
                "status": "error"
            }, 400


@latteh_app.route("/api/update_asset_checkin", methods=["POST"])
def update_asset_checkin():
    if request.method == 'POST':
        try:
            data = request.form.to_dict()
            print(f'DATA {data}')
            record = DB.company_assets.update_one({'asset_tag': data['asset_tag']}, {'$set': {'employee_list': data['owner_list'], 'asset_status': data['asset_status']}})
            if record:
                return {
                    "message": 'Asset checked in successfully',
                    "status": "success"
                }, 201
            else:
                return {
                    "message": 'Asset could not be checked in',
                    "status": "error"
                }, 400
        except Exception as exception:
            return {
                "message": exception,
                "status": "error"
            }, 400

from lattechHR import latteh_app, DB
from flask import render_template, session
from lattechHR.authentication import check_session_expiration
from lattechHR.employeeManagement import attendance
from datetime import datetime, date


@latteh_app.route("/dashboard")
@check_session_expiration
def dashboard():
    user = DB.users.find_one({'username': session['username']}, {'_id': 0, 'password': 0})
    attendance = DB.attendance.find_one({'username': session['username'], 'currentdate': datetime.now().strftime("%Y-%m-%d")}, {'_id': 0})
    
    current_date = str(date.today())
    cursor = DB.calendar_events.find({
    'from_date': {'$lte': current_date},
    'to_date': {'$gte': current_date}
    }, {'username': 1})
    usernames = [event['username'] for event in cursor]
    cursor = DB.users.find({'username': {'$in': usernames}}, {'fullname': 1, '_id': 0})
    leave_full_names = [doc['fullname'] for doc in cursor]
    
    cursor = DB.attendance.find({'currentdate': current_date, 'worklocation': 'home'})
    usernames = [doc['username'] for doc in cursor]
    cursor = DB.users.find({'username': {'$in': usernames}}, {'fullname': 1, '_id': 0})
    wfh_full_names = [doc['fullname'] for doc in cursor]
    
    leaves_data = {'general_leave': user['leaves']['general_leave'], 
                   'festival_leave': user['leaves']['festival_leave'],
                   'paid_leave': user['leaves']['paid_leave'],
                   'attendance': attendance,
                   'usernames_on_leave': leave_full_names,
                   'usernames_on_wfh': wfh_full_names,
                   'general_leave_percentage': round(((float(user['leaves']['general_leave'])/int(latteh_app.config['GENERAL_LEAVE']))*100), 2),
                   'festival_leave_percentage': round(((float(user['leaves']['festival_leave'])/int(latteh_app.config['FESTIVAL_LEAVE']))*100), 2)}
    return render_template('dashboard/dashboard.html', page='dashboard', leaves_data=leaves_data)
    


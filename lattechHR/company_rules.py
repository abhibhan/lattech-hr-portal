from lattechHR import latteh_app, DB
from flask import render_template, session, request, jsonify
from datetime import datetime
from bson.objectid import ObjectId
from bson.json_util import dumps
from lattechHR.authentication import check_session_expiration, admin_required


@latteh_app.route("/company_rules")
@check_session_expiration
def company_rules():
    return render_template('company_rules/home.html', page='Company Rules')

@latteh_app.route("/termination_policy")
@check_session_expiration
def termination_policy():
    return render_template('company_rules/termination_policy.html', page='Termination Policy')

@latteh_app.route("/code_of_conduct")
@check_session_expiration
def code_of_conduct():
    return render_template('company_rules/code_of_conduct.html', page='Code of conduct')

@latteh_app.route("/leave_policy")
@check_session_expiration
def leave_policy():
    return render_template('company_rules/leave_policy.html', page='Leave Policy')

@latteh_app.route("/wfh_policy")
@check_session_expiration
def wfh_policy():
    return render_template('company_rules/wfh_policy.html', page='WFH Policy')

@latteh_app.route("/employee_evaluation_rules")
@check_session_expiration
def employee_evaluation_rules():
    return render_template('company_rules/employee_evaluation.html', page='Employee Evaluation Policy')

@latteh_app.route("/non_descrimination")
@check_session_expiration
def non_descrimination():
    return render_template('company_rules/non_descrimination.html', page='Non Descrimination Policy')
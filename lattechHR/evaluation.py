from lattechHR import latteh_app, DB
from lattechHR.authentication import check_session_expiration, admin_required
from flask import request, jsonify, render_template, session, send_file

@latteh_app.route("/evaluation")
@check_session_expiration
def evaluation():
    data = DB.setting.find_one({}, {'_id': 0})
    return render_template('employees/evaluation.html', page='Evaluation', data=data)

@latteh_app.route("/self_evaluation")
@check_session_expiration
def self_evaluation():
    return render_template('employees/selfEvaluation.html', page='Self evaluation')

@latteh_app.route("/employee_evaluation")
@check_session_expiration
def employee_evaluation():
    return render_template('employees/companySelfEvaluation.html', page='Employee Evaluation')

@latteh_app.route("/api/update_self_evaluation", methods=["POST"])
@check_session_expiration
def update_self_evaluation():
    try:
        form_data = request.form.to_dict()
        filter_criteria = {'username': form_data['username'], 'cycle': form_data['cycle']}
        user = DB.users.find_one({'username': form_data['username']}, {'_id': 0, 'password': 0})
        del form_data['username']
        del form_data['cycle']
        data = {}
        data['evaluation'] = form_data
        data['username'] = user['username']
        data['fullname'] = user['fullname']
        data['email'] = user['email']
        data['phone'] = user['phone']
        data['designation'] = user['designation']

        result = DB.evaluation.update_one(
            filter_criteria,
            {'$set': data},
            upsert=True
        )
            
        if result:
            return {
                    "message": 'Evaluation data updated successfully',
                    "status": "success"
                }, 201
        else:
            return {
                    "message": "User record could not be added",
                    "status": "error"
                }, 400
    except Exception as exception:
        return {
            "message": exception,
            "status": "error"
        }, 400

@latteh_app.route("/api/get_self_evaluation", methods=["GET"])
@check_session_expiration
def get_self_evaluation():
    month_year = request.args.get('month_year')
    record = DB.evaluation.find_one({'username': session['username'], 'cycle': month_year}, {'_id': 0})
    print(record)
    return {
            "status": "success",
            "data": record['evaluation']
        }, 201
    
@latteh_app.route("/api/evaluation_table", methods=["GET"])
@check_session_expiration
def evaluation_table():
    month_year = request.args.get('month_year')
    if month_year != '':
        record = DB.evaluation.find_one({'username': session['username'], 'cycle': month_year}, {'_id': 0})
        evaluation_skills = ['analytical_skills','communication','decision_making','getting_work_done','technical_skills']
        employee_notes = ''
        if 'employee_notes' in record['evaluation']:
            employee_notes = record['evaluation']['employee_notes']
        data = []
        for skill in evaluation_skills:
            valuer = {'skill': skill.title().replace("_", " ")}
            for key, val in record['evaluation'].items():
                if '_company_rating' in key:
                    x = f'{skill}_company_rating'
                    print(f"X {x} - SKILL {skill}")
                    print(f"EV {x == key}")
                if f'{skill}_justification' == key:
                    valuer.update({'justification': val})
                if f'{skill}_rating' == key:
                    valuer.update({'rating': val})
                if f'{skill}_company_rating' == key:
                    valuer.update({'company_rating': val})
            # if f'{skill}_company_rating' not in key:
            #     valuer.update({'company_rating': 'NA'})
            data.append(valuer)    
        return {
                "status": "success",
                "data": data,
                "employee_notes": employee_notes
            }, 201
    else:
        return {
                "status": "success",
                "data": []
            }, 201

@latteh_app.route("/api/all_cycles", methods=["GET"])
@check_session_expiration
def all_cycles():
    try:
        records = DB.evaluation.find({'username': session['username']}, {'_id': 0, 'cycle': 1})
        all_cycles = []
        for record in records:
            if 'cycle' in record:
                all_cycles.append(record['cycle'])
        return {
                "status": "success",
                "data": all_cycles
            }, 201
    except Exception as exception:
        return {
            "message": exception,
            "status": "error"
        }, 400

@latteh_app.route("/api/unique_cycles", methods=["GET"])
@check_session_expiration
def unique_cycles():
    try:
        records = DB.evaluation.distinct('cycle')
        return {
                "status": "success",
                "data": records
            }, 201
    except Exception as exception:
        return {
            "message": exception,
            "status": "error"
        }, 400
        
@latteh_app.route("/api/get_employee_self_evaluation", methods=["GET"])
@check_session_expiration
@admin_required
def get_employee_self_evaluation():
    month_year = request.args.get('month_year')
    username = request.args.get('username')
    try:
        if DB.evaluation.count_documents({'username': username, 'cycle': month_year}) > 0:
            record = DB.evaluation.find_one({'username': username, 'cycle': month_year}, {'_id': 0})
            return {
                    "status": "success",
                    "data": record['evaluation']
                }, 201
        else:
            return {
                    "status": "error",
                    "data": []
                }, 400
    except Exception as exception:
        return {
            "message": exception,
            "status": "error"
        }, 400
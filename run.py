from lattechHR import latteh_app

if __name__ == '__main__':
    latteh_app.run(debug=latteh_app.config['DEBUG'], host='0.0.0.0', port=8000, threaded=True)
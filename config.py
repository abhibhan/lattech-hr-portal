from os import environ, path, getenv
from dotenv import load_dotenv
load_dotenv()

class Config:
    APP_ROOT = path.dirname(path.abspath(__file__))
    DEBUG = True
    IMAGE_FOLDER = 'static/images/upload/'
    DB_DSN=getenv('DB_DSN')
    SECRET_KEY=getenv('SECRET_KEY')
    JWT_TOKEN_VALID_MINS=getenv('JWT_TOKEN_VALID_MINS')
    USER_PROFILE_PHOTO=path.join(APP_ROOT, 'uploads', 'user_profile')
    PAYSLIP_DIRECTORY=path.join(APP_ROOT, 'uploads', 'payslip')
    GENERAL_LEAVE=getenv('GENERAL_LEAVE')
    FESTIVAL_LEAVE=getenv('FESTIVAL_LEAVE')
    WORK_FROM_HOME=getenv('WORK_FROM_HOME')
    GENERAL_LEAVE_PER_MONTH=getenv('GENERAL_LEAVE_PER_MONTH')
    WORK_FROM_HOME_PER_MONTH=getenv('WORK_FROM_HOME_PER_MONTH')
    EMAIL=getenv('EMAIL')
    EMAIL_PASSWORD=getenv('EMAIL_PASSWORD')
    SMTP=getenv('SMTP')
    ORIGINAL_URL=getenv('ORIGINAL_URL')
    ADMIN_EMAIL=getenv('ADMIN_EMAIL')
    TEAMS_WEBHOOK=getenv('TEAMS_WEBHOOK')
    OPEN_AI_API=getenv('OPEN_AI_API')
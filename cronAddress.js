const moment = require('moment-timezone');

function printServerAndISTTime() {
  // Set the time zone to Indian Standard Time (IST)
  const istTimeZone = 'Asia/Kolkata';

  // Get the current date and time in the server's local time
  const currentServerTime = moment();

  // Convert the server time to Indian Standard Time (IST)
  const currentISTTime = currentServerTime.tz(istTimeZone);
  console.log('Server Time:', currentServerTime.format('YYYY-MM-DD HH:mm:ss'));
  console.log('Indian Standard Time (IST):', currentISTTime.format('YYYY-MM-DD HH:mm:ss'));
}

// Call the function
printServerAndISTTime();
